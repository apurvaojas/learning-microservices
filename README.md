# Food Delivery App

# [Click Here For Assignment README](./kubernetes-assignemt.md)

## Project Structure

This repo is following mono repo pattern.

- /apps
  - /user-service
  - /restaurant-service

## How to Run

`docker-compose up`

## User Service [http://localhost:8082/explorer/]

- User service is built using [`@nestjs`](https://nestjs.com/)
- Persistent Dta Model MongoDb
  - ORM/ODM - `Mongoose`
- To run test `npm run test:e2e` after `npm install`
- [Docker image Hub](https://hub.docker.com/repository/docker/apurvaojas10/user-service)

## Restaurant Service [http://localhost:8081/explorer/]

- User service is built using [`@loopback`](https://loopback.io/doc/en/lb4/)
- Persistent Dta Model MongoDb
  - ORM/ODM - `@loopback/repository`
- Test cases yet to be implemented
- [Docker image Hub](https://hub.docker.com/repository/docker/apurvaojas10/restaurant-service)

## Roadmap

- Test Coverage in restaurant-service
- Nx Monorepo setup
- CI/CD
- Messaging
- Migrations Sample data
