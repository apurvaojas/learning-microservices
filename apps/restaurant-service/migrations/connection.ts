import { MongoClient } from 'mongodb';
import { getMongoDbUrl } from '../src/datasources';

const delay = (ms = 0) => {
  return new Promise((resolve, reject) => setTimeout(resolve, ms));
};

export const getDb = async () => {
  // Awaiting to boot mongoDb
  await delay(5000);
  const client: MongoClient = await MongoClient.connect(getMongoDbUrl());
  return client.db();
};
