/* eslint-disable @typescript-eslint/naming-convention */
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {Cusines} from '../models';
import {CusinesRepository} from '../repositories';

export class CusinesController {
  constructor(
    @repository(CusinesRepository)
    public cusinesRepository: CusinesRepository,
  ) {}

  @post('/cusines')
  @response(200, {
    description: 'Cusines model instance',
    content: {'application/json': {schema: getModelSchemaRef(Cusines)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Cusines, {
            title: 'NewCusines',
          }),
        },
      },
    })
    cusines: Cusines,
  ): Promise<Cusines> {
    return this.cusinesRepository.create(cusines);
  }

  @get('/cusines/count')
  @response(200, {
    description: 'Cusines model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Cusines) where?: Where<Cusines>): Promise<Count> {
    return this.cusinesRepository.count(where);
  }

  @get('/cusines')
  @response(200, {
    description: 'Array of Cusines model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Cusines, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Cusines) filter?: Filter<Cusines>,
  ): Promise<Cusines[]> {
    return this.cusinesRepository.find(filter);
  }

  // @patch('/cusines')
  // @response(200, {
  //   description: 'Cusines PATCH success count',
  //   content: {'application/json': {schema: CountSchema}},
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Cusines, {partial: true}),
  //       },
  //     },
  //   })
  //   cusines: Cusines,
  //   @param.where(Cusines) where?: Where<Cusines>,
  // ): Promise<Count> {
  //   return this.cusinesRepository.updateAll(cusines, where);
  // }

  @get('/cusines/{id}')
  @response(200, {
    description: 'Cusines model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Cusines, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Cusines, {exclude: 'where'})
    filter?: FilterExcludingWhere<Cusines>,
  ): Promise<Cusines> {
    return this.cusinesRepository.findById(id, filter);
  }

  @patch('/cusines/{id}')
  @response(204, {
    description: 'Cusines PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Cusines, {partial: true}),
        },
      },
    })
    cusines: Cusines,
  ): Promise<void> {
    await this.cusinesRepository.updateById(id, cusines);
  }

  // @put('/cusines/{id}')
  // @response(204, {
  //   description: 'Cusines PUT success',
  // })
  // async replaceById(
  //   @param.path.string('id') id: string,
  //   @requestBody() cusines: Cusines,
  // ): Promise<void> {
  //   await this.cusinesRepository.replaceById(id, cusines);
  // }

  @del('/cusines/{id}')
  @response(204, {
    description: 'Cusines DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.cusinesRepository.deleteById(id);
  }
}
