import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {FoodCategory} from '../models';
import {FoodCategoryRepository} from '../repositories';

export class FoodCategoryController {
  constructor(
    @repository(FoodCategoryRepository)
    public foodCategoryRepository : FoodCategoryRepository,
  ) {}

  @post('/food-categories')
  @response(200, {
    description: 'FoodCategory model instance',
    content: {'application/json': {schema: getModelSchemaRef(FoodCategory)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FoodCategory, {
            title: 'NewFoodCategory',
            
          }),
        },
      },
    })
    foodCategory: FoodCategory,
  ): Promise<FoodCategory> {
    return this.foodCategoryRepository.create(foodCategory);
  }

  @get('/food-categories/count')
  @response(200, {
    description: 'FoodCategory model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(FoodCategory) where?: Where<FoodCategory>,
  ): Promise<Count> {
    return this.foodCategoryRepository.count(where);
  }

  @get('/food-categories')
  @response(200, {
    description: 'Array of FoodCategory model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(FoodCategory, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(FoodCategory) filter?: Filter<FoodCategory>,
  ): Promise<FoodCategory[]> {
    return this.foodCategoryRepository.find(filter);
  }

  @patch('/food-categories')
  @response(200, {
    description: 'FoodCategory PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FoodCategory, {partial: true}),
        },
      },
    })
    foodCategory: FoodCategory,
    @param.where(FoodCategory) where?: Where<FoodCategory>,
  ): Promise<Count> {
    return this.foodCategoryRepository.updateAll(foodCategory, where);
  }

  @get('/food-categories/{id}')
  @response(200, {
    description: 'FoodCategory model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(FoodCategory, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(FoodCategory, {exclude: 'where'}) filter?: FilterExcludingWhere<FoodCategory>
  ): Promise<FoodCategory> {
    return this.foodCategoryRepository.findById(id, filter);
  }

  @patch('/food-categories/{id}')
  @response(204, {
    description: 'FoodCategory PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FoodCategory, {partial: true}),
        },
      },
    })
    foodCategory: FoodCategory,
  ): Promise<void> {
    await this.foodCategoryRepository.updateById(id, foodCategory);
  }

  @put('/food-categories/{id}')
  @response(204, {
    description: 'FoodCategory PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() foodCategory: FoodCategory,
  ): Promise<void> {
    await this.foodCategoryRepository.replaceById(id, foodCategory);
  }

  @del('/food-categories/{id}')
  @response(204, {
    description: 'FoodCategory DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.foodCategoryRepository.deleteById(id);
  }
}
