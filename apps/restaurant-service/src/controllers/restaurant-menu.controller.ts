/* eslint-disable @typescript-eslint/naming-convention */
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Restaurant,
  Menu,
} from '../models';
import {RestaurantRepository} from '../repositories';

export class RestaurantMenuController {
  constructor(
    @repository(RestaurantRepository) protected restaurantRepository: RestaurantRepository,
  ) { }

  @get('/restaurants/{id}/menu', {
    responses: {
      '200': {
        description: 'Restaurant has one Menu',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Menu),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Menu>,
  ): Promise<Menu> {
    return this.restaurantRepository.menu(id).get(filter);
  }

  @post('/restaurants/{id}/menu', {
    responses: {
      '200': {
        description: 'Restaurant model instance',
        content: {'application/json': {schema: getModelSchemaRef(Menu)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Restaurant.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menu, {
            title: 'NewMenuInRestaurant',
            exclude: ['id'],
            optional: ['restaurantId']
          }),
        },
      },
    }) menu: Omit<Menu, 'id'>,
  ): Promise<Menu> {
    return this.restaurantRepository.menu(id).create(menu);
  }

  @patch('/restaurants/{id}/menu', {
    responses: {
      '200': {
        description: 'Restaurant.Menu PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Menu, {partial: true}),
        },
      },
    })
    menu: Partial<Menu>,
    @param.query.object('where', getWhereSchemaFor(Menu)) where?: Where<Menu>,
  ): Promise<Count> {
    return this.restaurantRepository.menu(id).patch(menu, where);
  }

  @del('/restaurants/{id}/menu', {
    responses: {
      '200': {
        description: 'Restaurant.Menu DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Menu)) where?: Where<Menu>,
  ): Promise<Count> {
    return this.restaurantRepository.menu(id).delete(where);
  }
}
