import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
import process from 'process';

const {
      DB_CONFIG_MONGODB_HOST = 'localhost',
      DB_CONFIG_MONGODB_PORT = 27017,
      DB_CONFIG_MONGODB_USER = 'testUser',
      DB_CONFIG_MONGODB_PASSWORD = 'simple123',
      DB_CONFIG_MONGODB_DATABASE = 'restaurantDb'
} = process.env;


const config = {
  name: 'RestaurantSrviceDb',
  connector: 'mongodb',
  url: '',
  host: DB_CONFIG_MONGODB_HOST,
  port: DB_CONFIG_MONGODB_PORT,
  user: DB_CONFIG_MONGODB_USER,
  password: DB_CONFIG_MONGODB_PASSWORD,
  database: DB_CONFIG_MONGODB_DATABASE,
  useNewUrlParser: true
};

export const getMongoDbUrl = () =>
  `mongodb://${DB_CONFIG_MONGODB_USER}:${DB_CONFIG_MONGODB_PASSWORD}@${DB_CONFIG_MONGODB_HOST}:${DB_CONFIG_MONGODB_PORT}/${DB_CONFIG_MONGODB_DATABASE}`;




// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class RestaurantSrviceDbDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'RestaurantSrviceDb';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.RestaurantSrviceDb', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
