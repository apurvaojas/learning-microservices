import {Provider, inject} from '@loopback/core';
import {ReadyCheck} from '@loopback/health';
import {DataSource} from '@loopback/repository';

export class DBHealthCheckProvider implements Provider<ReadyCheck> {
  constructor(@inject('datasources.RestaurantSrviceDb') private ds: DataSource) {}

  value() {
    return () => this.ds.ping();
  }
}
