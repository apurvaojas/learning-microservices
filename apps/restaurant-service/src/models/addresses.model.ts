import {model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Addresses {


  @property({
    type: 'array',
    itemType: 'string',
    required: true,
  })
  addressLines: string[];

  @property({
    type: 'string',
    required: true,
  })
  city: string;

  @property({
    type: 'string',
    required: true,
  })
  state: string;

  @property({
    type: 'string',
    required: true,
  })
  pincode: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Addresses>) {
    
  }
}

export interface AddressesRelations {
  // describe navigational properties here
}

export type AddressesWithRelations = Addresses & AddressesRelations;
