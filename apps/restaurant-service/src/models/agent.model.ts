import {model, property} from '@loopback/repository';

enum EAgentPriVilages {
  ADMIN = 'ADMIN',
  STAFF = 'STAFF',
}

@model({settings: {strict: false}})
export class Agent {
  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'string',
    required: true,
  })
  privilage: EAgentPriVilages;

  @property({
    type: 'string',
    required: true,
  })
  name: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Agent>) {}
}

export interface AgentRelations {
  // describe navigational properties here
}

export type AgentWithRelations = Agent & AgentRelations;
