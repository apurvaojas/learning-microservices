import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Cusines, CusinesWithRelations} from './cusines.model';
import {FoodCategory, FoodCategoryWithRelations} from './food-category.model';

@model({settings: {strict: false}})
export class Dishes extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  desc?: string;

  @property({
    type: 'string',
  })
  image?: string;

  @belongsTo(() => Cusines)
  cusinesId: string;

  @belongsTo(() => FoodCategory)
  foodCategoryId: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Dishes>) {
    super(data);
  }
}

export interface DishesRelations {
  // describe navigational properties here
  cusine?: CusinesWithRelations,
  foodCategory?: FoodCategoryWithRelations
}

export type DishesWithRelations = Dishes & DishesRelations;
