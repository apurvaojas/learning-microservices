import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class FoodCategory extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  desc?: string;

  // @hasMany(() => Dishes)
  // dishes: Dishes[];

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<FoodCategory>) {
    super(data);
  }
}

export interface FoodCategoryRelations {
  // describe navigational properties here
  // dishes?: DishesWithRelations[]
}

export type FoodCategoryWithRelations = FoodCategory & FoodCategoryRelations;
