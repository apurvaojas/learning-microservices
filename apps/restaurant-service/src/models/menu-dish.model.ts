import {hasOne, model, property} from '@loopback/repository';
import {Dishes} from './dishes.model';

@model({settings: {strict: false}})
export class MenuDish {
  @property({
    type: 'number',
    required: true,
  })
  stock: number;

  @property({
    type: 'number',
    required: true,
  })
  price: number;

  // @hasOne(() => Dishes, {
  //   keyTo: 'id'
  // })
  @hasOne(() => Dishes)
  dishId: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<MenuDish>) {}
}

export interface MenuDishRelations {
  // describe navigational properties here
}

export type MenuDishWithRelations = MenuDish & MenuDishRelations;
