import {belongsTo, Entity, model, property} from '@loopback/repository';
import {MenuDish} from './menu-dish.model';
import {Restaurant} from './restaurant.model';

@model({settings: {strict: false}})
export class Menu extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property.array(MenuDish)
  dishes: MenuDish[];

  @belongsTo(() => Restaurant, {name: 'restaurant'})
  restaurantId: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Menu>) {
    super(data);
  }
}

export interface MenuRelations {
  // describe navigational properties here
}

export type MenuWithRelations = Menu & MenuRelations;
