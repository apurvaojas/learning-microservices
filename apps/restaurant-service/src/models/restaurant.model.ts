import {Entity, hasOne, model, property} from '@loopback/repository';
import {Addresses} from './addresses.model';
import {Agent} from './agent.model';
import {Menu} from './menu.model';

export interface GeoPoint {
  lat: number;
  lng: number;
}

@model({settings: {strict: false}})
export class Restaurant extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'object',
    required: true,
    mongodb: {dataType: 'Point'}
  })
  location: GeoPoint;

  @property({
    type: 'number',
    required: true,
  })
  serviceRadius: number;

  @property.array(Agent)
  agents: Agent[];



  @property({
    type: Addresses,
    required: true
  })
  address: Addresses

  @hasOne(() => Menu)
  menu: Menu;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Restaurant>) {
    super(data);
  }
}

export interface RestaurantRelations {
  // describe navigational properties here
}

export type RestaurantWithRelations = Restaurant & RestaurantRelations;
