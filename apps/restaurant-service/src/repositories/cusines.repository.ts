import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {RestaurantSrviceDbDataSource} from '../datasources';
import {Cusines, CusinesRelations} from '../models';

export class CusinesRepository extends DefaultCrudRepository<
  Cusines,
  typeof Cusines.prototype.id,
  CusinesRelations
> {
  constructor(
    @inject('datasources.RestaurantSrviceDb') dataSource: RestaurantSrviceDbDataSource,
  ) {
    super(Cusines, dataSource);
  }
}
