import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  Filter,
  Options,
  repository,
} from '@loopback/repository';
import {RestaurantSrviceDbDataSource} from '../datasources';
import {
  Cusines,
  Dishes,
  DishesRelations,
  DishesWithRelations,
  FoodCategory,
} from '../models';
import {CusinesRepository} from './cusines.repository';
import {FoodCategoryRepository} from './food-category.repository';

export class DishesRepository extends DefaultCrudRepository<
  Dishes,
  typeof Dishes.prototype.id,
  DishesRelations
> {
  public readonly cusines: BelongsToAccessor<
    Cusines,
    typeof Dishes.prototype.id
  >;

  public readonly foodCategory: BelongsToAccessor<
    FoodCategory,
    typeof Dishes.prototype.id
  >;

  constructor(
    @inject('datasources.RestaurantSrviceDb')
    dataSource: RestaurantSrviceDbDataSource,
    @repository.getter('CusinesRepository')
    protected cusinesRepositoryGetter: Getter<CusinesRepository>,
    @repository.getter('FoodCategoryRepository')
    protected foodCategoryRepositoryGetter: Getter<FoodCategoryRepository>,
  ) {
    super(Dishes, dataSource);
    this.foodCategory = this.createBelongsToAccessorFor(
      'foodCategory',
      foodCategoryRepositoryGetter,
    );
    this.registerInclusionResolver(
      'foodCategory',
      this.foodCategory.inclusionResolver,
    );
    this.cusines = this.createBelongsToAccessorFor(
      'cusines',
      cusinesRepositoryGetter,
    );
    this.registerInclusionResolver('cusines', this.cusines.inclusionResolver);
  }

  async find(
    filter?: Filter<Dishes>,
    options?: Options,
  ): Promise<DishesWithRelations[]> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const collection = (this.dataSource.connector as any).collection('Dishes');
    const values = await collection
      .aggregate([
        {
          $lookup: {
            from: 'Cusines',
            localField: 'cusinesId',
            foreignField: '_id',
            as: 'cusine',
          },
        },
        {
          $lookup: {
            from: 'FoodCategory',
            localField: 'foodCategoryId',
            foreignField: '_id',
            as: 'foodCategory',
          },
        },
        {$unwind: '$cusine'},
        {$unwind: '$foodCategory'},
        {$project: {cusinesId: 0, foodCategoryId: 0}},
      ])
      .get();

    return values;
  }
}
