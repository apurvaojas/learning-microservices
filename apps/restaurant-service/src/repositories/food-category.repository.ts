import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {RestaurantSrviceDbDataSource} from '../datasources';
import {FoodCategory, FoodCategoryRelations} from '../models';

export class FoodCategoryRepository extends DefaultCrudRepository<
  FoodCategory,
  typeof FoodCategory.prototype.id,
  FoodCategoryRelations
> {
  constructor(
    @inject('datasources.RestaurantSrviceDb') dataSource: RestaurantSrviceDbDataSource,
  ) {
    super(FoodCategory, dataSource);
  }
}
