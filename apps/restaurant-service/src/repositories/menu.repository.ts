import {inject} from '@loopback/core';
import {DefaultCrudRepository, Filter, Options} from '@loopback/repository';
import {RestaurantSrviceDbDataSource} from '../datasources';
import {Menu, MenuRelations, MenuWithRelations} from '../models';

export class MenuRepository extends DefaultCrudRepository<
  Menu,
  typeof Menu.prototype.id,
  MenuRelations
> {
  constructor(
    @inject('datasources.RestaurantSrviceDb')
    dataSource: RestaurantSrviceDbDataSource,
  ) {
    super(Menu, dataSource);
  }

  async find(
    filter?: Filter<Menu>,
    options?: Options,
  ): Promise<MenuWithRelations[]> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const collection = (this.dataSource.connector as any).collection('Menu');
    const values = await collection
      .aggregate([
        {$unwind: '$dishes'},
        {
          $lookup: {
            from: 'Dishes',
            localField: 'dishes.dishId',
            foreignField: '_id',
            as: 'dish',
          },
        },
        {$unwind: '$dish'},
        {$project: {
          _id: 1,
          name: 1,
          dishes: [
            {
              stock: '$dishes.stock',
              price: '$dishes.price',
              dish: '$dish'
            }
          ]
        }},
      ])
      .get();
    return values;
  }
}
