import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasOneRepositoryFactory} from '@loopback/repository';
import {RestaurantSrviceDbDataSource} from '../datasources';
import {Restaurant, RestaurantRelations, Menu} from '../models';
import {MenuRepository} from './menu.repository';

export class RestaurantRepository extends DefaultCrudRepository<
  Restaurant,
  typeof Restaurant.prototype.id,
  RestaurantRelations
> {

  public readonly menu: HasOneRepositoryFactory<Menu, typeof Restaurant.prototype.id>;

  constructor(
    @inject('datasources.RestaurantSrviceDb') dataSource: RestaurantSrviceDbDataSource,
    @repository.getter('MenuRepository') protected menuRepositoryGetter: Getter<MenuRepository>,
  ) {
    super(Restaurant, dataSource);
    this.menu = this.createHasOneRepositoryFactoryFor('menu', menuRepositoryGetter);
    this.registerInclusionResolver('menu', this.menu.inclusionResolver);
  }
}
