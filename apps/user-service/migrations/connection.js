"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDb = void 0;
const mongodb_1 = require("mongodb");
const helper_1 = require("../src/helper");
const delay = (ms = 0) => {
    return new Promise((resolve, reject) => setTimeout(resolve, ms));
};
const getDb = async () => {
    await delay(5000);
    const client = await mongodb_1.MongoClient.connect((0, helper_1.getMongoDbUrl)());
    return client.db();
};
exports.getDb = getDb;
//# sourceMappingURL=connection.js.map