"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
const connection_1 = require("../connection");
const up = async () => {
    const db = await (0, connection_1.getDb)();
    const collection = db.collection('users');
    const insertResult = await collection.insertMany([
        {
            addresses: [
                {
                    addressLines: ['address Line 1', 'address Line 2'],
                    city: 'Delhi',
                    location: [28.4831198, 77.0032016],
                    pin: '110075',
                    state: 'Delhi',
                },
            ],
            name: 'My Restaurant',
            email: 'hello@my.restaurant',
            phone: '123456789',
            password: 'simple123',
        },
        {
            addresses: [
                {
                    addressLines: ['address Line 1', 'address Line 2'],
                    city: 'Gurgaon',
                    location: [28.4831198, 77.0032016],
                    pin: '122006',
                    state: 'Haryana',
                },
            ],
            name: 'My Gurgaon Restaurant',
            email: 'hello.gurgaon@my.restaurant',
            phone: '123456789233',
            password: 'simple123',
        },
    ]);
    console.log('Inserted documents =>', insertResult);
};
exports.up = up;
const down = async () => {
    const db = await (0, connection_1.getDb)();
};
exports.down = down;
//# sourceMappingURL=1652077276804-add-users.js.map