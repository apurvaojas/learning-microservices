/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-ignore
import { getDb } from '../connection';

export const up = async () => {
  const db = await getDb();
  /*
      Code your update script here!
   */
  const collection = db.collection('users');
  const insertResult = await collection.insertMany([
    {
      addresses: [
        {
          addressLines: ['address Line 1', 'address Line 2'],
          city: 'Delhi',
          location: [28.4831198, 77.0032016],
          pin: '110075',
          state: 'Delhi',
        },
      ],
      name: 'My Restaurant',
      email: 'hello@my.restaurant',
      phone: '123456789',
      password: 'simple123',
    },
    {
      addresses: [
        {
          addressLines: ['address Line 1', 'address Line 2'],
          city: 'Gurgaon',
          location: [28.4831198, 77.0032016],
          pin: '122006',
          state: 'Haryana',
        },
      ],
      name: 'My Gurgaon Restaurant',
      email: 'hello.gurgaon@my.restaurant',
      phone: '123456789233',
      password: 'simple123',
    },

    //  { a: 2 },
    //  { a: 3 },
  ]);
  console.log('Inserted documents =>', insertResult);
};

export const down = async () => {
  const db = await getDb();
  /*
      Code you downgrade script here!
   */
};
