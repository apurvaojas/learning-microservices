import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { getMongoDbUrl } from './helper';
import { UsersModule } from './users/users.module';
import { HealthModule } from './health/health.module';

@Module({
  imports: [MongooseModule.forRoot(getMongoDbUrl()), UsersModule, HealthModule],
})
export class AppModule {}
