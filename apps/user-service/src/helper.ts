import { NextFunction, Request, Response } from 'express';
import * as process from 'process';

const {
  DB_CONFIG_MONGODB_HOST = 'localhost',
  DB_CONFIG_MONGODB_PORT = 27017,
  DB_CONFIG_MONGODB_USER = 'testUser1',
  DB_CONFIG_MONGODB_PASSWORD = 'simple123',
  DB_CONFIG_MONGODB_DATABASE = 'usersDb',
} = process.env;

export const getMongoDbUrl = () =>
  `mongodb://${DB_CONFIG_MONGODB_USER}:${DB_CONFIG_MONGODB_PASSWORD}@${DB_CONFIG_MONGODB_HOST}:${DB_CONFIG_MONGODB_PORT}/${DB_CONFIG_MONGODB_DATABASE}`;

export { DB_CONFIG_MONGODB_DATABASE };

export function forwardedPrefixMiddleware(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  req.originalUrl = (req.headers['x-forwarded-prefix'] || '') + req.url;
  next();
}
