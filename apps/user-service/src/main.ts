import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as fs from 'fs';
// import { AllExceptionsFilter } from './app.exceptionFilter';
import { AppModule } from './app.module';
import { forwardedPrefixMiddleware } from './helper';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('User Services')
    .setDescription('The Users API description')
    .setVersion('1.0')
    .addTag('users')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  fs.writeFileSync('../swagger-spec.json', JSON.stringify(document));

  app.use(forwardedPrefixMiddleware);

  SwaggerModule.setup('explorer', app, document);

  // const { httpAdapter } = app.get(HttpAdapterHost);
  // app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  await app.listen(8083);
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
