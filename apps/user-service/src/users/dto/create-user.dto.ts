import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsEmail,
  IsNumberString,
  IsPostalCode,
  IsString,
  ValidateNested,
} from 'class-validator';

// const AddressLines: string[] = [];
export interface IAddress {
  addressLines: string[];
  city: string;
  state: string;
  pin: number;
  location: number[];
}

export enum EUserTypes {
  RESTAURANT_ADMIN = 'RESTAURANT_ADMIN',
  RESTAURANT_MANAGER = 'RESTAURANT_MANAGER',
  DELIVERY_PARTNER = 'DELIVERY_PARTNER',
  END_CONSUMER = 'END_CONSUMER',
}

export class CreateUserDto {
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsNumberString()
  phone: string;

  type?: EUserTypes;

  @IsString()
  password: string;

  @ValidateNested({ each: true })
  @Type(() => Address)
  @ApiProperty({ type: () => Address, isArray: true })
  addresses: Address[];
}

class Address implements IAddress {
  addressLines: string[];

  @IsString()
  city: string;

  location: number[];

  @IsPostalCode('IN')
  pin: number;

  @IsString()
  state: string;
}
