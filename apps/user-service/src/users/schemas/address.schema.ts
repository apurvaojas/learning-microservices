import 'mongoose-geojson-schema';
import { Prop } from '@nestjs/mongoose';
import { Schema as MongooseSchema } from 'mongoose';
import { IAddress } from '../dto/create-user.dto';

export class Address implements IAddress {
  @Prop({ type: [String], required: true, lowercase: true })
  addressLines: string[];

  @Prop({ required: true })
  pin: number;

  @Prop({ required: true })
  city: string;

  @Prop({ required: true })
  state: string;

  @Prop({ required: true, type: MongooseSchema.Types.Point })
  location: number[];
}
