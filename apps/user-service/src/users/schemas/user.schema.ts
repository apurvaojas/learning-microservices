import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document, Schema as MongooseSchema } from 'mongoose';
import { EUserTypes } from '../dto/create-user.dto';
import { Address } from './address.schema';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
  })
  id: string;

  @Prop({
    required: true,
  })
  name: string;

  @Prop({
    required: true,
  })
  email: string;

  @Prop({
    required: true,
  })
  phone: string;

  @Prop({
    default: EUserTypes.END_CONSUMER,
  })
  type: EUserTypes;

  @Prop({
    required: true,
  })
  password: string;

  @Prop({ type: MongooseSchema.Types.Array, required: true })
  addresses: Address[];
}

export const UserSchema = SchemaFactory.createForClass(User);
