import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Res,
} from '@nestjs/common';
import {
  ApiAcceptedResponse,
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './schemas/user.schema';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: User,
  })
  @ApiBadRequestResponse({
    description: 'Invalid Data Type',
  })
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  @ApiNoContentResponse()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  @ApiNoContentResponse({ description: 'No Content' })
  async findOne(@Param('id') id: string, @Res() response) {
    const item = await this.usersService.findOne(id);
    if (!item) {
      response.status(HttpStatus.NO_CONTENT).send();
      return;
    }
    return response.status(HttpStatus.OK).send(item);
  }

  @Patch(':id')
  @ApiOkResponse()
  @ApiBadRequestResponse({
    description: 'Invalid Data Type',
  })
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  @ApiAcceptedResponse({
    description: 'Request Accepted',
  })
  @HttpCode(HttpStatus.ACCEPTED)
  remove(@Param('id') id: string) {
    return this.usersService.remove(id);
  }
}
