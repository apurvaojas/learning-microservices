import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let _id: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    return {};
  });

  afterAll((done) => {
    app.close();
    done();
  });

  it('/users (POST) responds with 400', async function () {
    const resp = await request(app.getHttpServer())
      .post('/users')
      .send({ name: 'john' });

    expect(resp.status).toEqual(400);
    expect(resp.body).toEqual({
      statusCode: 400,
      message: [
        'email must be an email',
        'phone must be a number string',
        'password must be a string',
      ],
      error: 'Bad Request',
    });

    return {};
  });

  it('/users (POST) responds with the data added', async function () {
    const resp = await request(app.getHttpServer())
      .post('/users')
      .send({
        addresses: [
          {
            addressLines: ['address Line 1', 'address Line 2'],
            city: 'Delhi',
            location: [28.4831198, 77.0032016],
            pin: '110075',
            state: 'Delhi',
          },
        ],
        name: 'MY test reastaurant',
        email: 'hello@my.restaurant',
        phone: '123456789',
        password: 'simple123',
      });

    expect(resp.status).toEqual(201);
    expect(resp.body.name).toEqual('MY test reastaurant');

    _id = resp.body._id;

    return {};
  });

  it('/users (GET) users to have length 2', async () => {
    const response = await request(app.getHttpServer())
      .get('/users')
      .set('Accept', 'application/json');
    expect(response.status).toEqual(200);
    expect(response.body.length).toEqual(1);
    return {};
  });

  it('/users (GET) should have data from fixture', async () => {
    const response = await request(app.getHttpServer())
      .get('/users')
      .set('Accept', 'application/json');
    expect(response.status).toEqual(200);
    expect(
      response.body.find((i) => i.name === 'MY test reastaurant'),
    ).not.toBe(undefined);
    return {};
  });

  it('/users/:id (GET) should return the specific user', async () => {
    const response = await request(app.getHttpServer())
      .get(`/users/${_id}`)
      .set('Accept', 'application/json');
    expect(response.status).toEqual(200);
    expect(response.body._id).toEqual(_id);
    return {};
  });

  it('/users/:id (GET) should return the specific user', async () => {
    const response = await request(app.getHttpServer())
      .get(`/users/${_id}`)
      .set('Accept', 'application/json');
    expect(response.status).toEqual(200);
    expect(response.body._id).toEqual(_id);
    return {};
  });

  it('/users/:id (GET) should return 204 for invalid user id', async () => {
    const response = await request(app.getHttpServer())
      .get(`/users/6277ca88faa7eb5cb19c9bec`)
      .set('Accept', 'application/json');
    expect(response.status).toEqual(204);
    return {};
  });

  it('/users/:id (PATCH) should update the name of give user', async () => {
    const response = await request(app.getHttpServer())
      .patch(`/users/${_id}`)
      .send({ name: 'My Restaurant New Name' })
      .set('Accept', 'application/json');
    expect(response.status).toEqual(200);
    expect(response.body.name).toEqual('My Restaurant New Name');
    return {};
  });

  it('/users/:id (PATCH) should return 400 for updating invalid email', async () => {
    const response = await request(app.getHttpServer())
      .patch(`/users/${_id}`)
      .send({ email: 'My Restaurant New Name' })
      .set('Accept', 'application/json');
    expect(response.status).toEqual(400);
    // expect(response.body.name).toEqual('My Restaurant New Name');
    return {};
  });

  it('/users/:id (DELETE) should delete user specified by id', async () => {
    const response = await request(app.getHttpServer()).delete(`/users/${_id}`);

    expect(response.status).toEqual(202);
    // expect(response.body).toEqual('My Restaurant New Name');
    return {};
  });
});
