


# AWS EKS Logs

## `kubectl get pods,svc,deploy,ingress,pv,pvc -A`

```
NAMESPACE           NAME                                      READY   STATUS    RESTARTS   AGE
food-delivery-app   pod/mongo-0                               1/1     Running   0          6m32s
food-delivery-app   pod/mongo-1                               1/1     Running   0          7m19s
food-delivery-app   pod/mongo-2                               1/1     Running   0          6m56s
food-delivery-app   pod/restaurant-service-6b5ffc9d5-27k4w    1/1     Running   0          7m19s
food-delivery-app   pod/restaurant-service-6b5ffc9d5-sjf5k    1/1     Running   0          7m19s
food-delivery-app   pod/restaurant-service-6b5ffc9d5-vsvmt    1/1     Running   0          14m
food-delivery-app   pod/user-service-dd7c5fc97-grqmb          1/1     Running   0          7m19s
food-delivery-app   pod/user-service-dd7c5fc97-j9wng          1/1     Running   0          7m19s
food-delivery-app   pod/user-service-dd7c5fc97-nqtrl          1/1     Running   0          14m
kong                pod/ingress-kong-9775c687c-knmjh          2/2     Running   0          19m
kube-system         pod/aws-node-44hw4                        1/1     Running   0          3h44m
kube-system         pod/aws-node-5tkjl                        1/1     Running   0          3h44m
kube-system         pod/aws-node-7rwc9                        1/1     Running   0          3h44m
kube-system         pod/coredns-7f5998f4c-jk64n               1/1     Running   0          3h53m
kube-system         pod/coredns-7f5998f4c-mgrwm               1/1     Running   0          3h53m
kube-system         pod/ebs-csi-controller-7bf6fbf96c-kpdsl   6/6     Running   0          97m
kube-system         pod/ebs-csi-controller-7bf6fbf96c-pj8nf   6/6     Running   0          97m
kube-system         pod/ebs-csi-node-c4sjs                    3/3     Running   0          97m
kube-system         pod/ebs-csi-node-lt4qk                    3/3     Running   0          97m
kube-system         pod/ebs-csi-node-vqfq8                    3/3     Running   0          97m
kube-system         pod/ebs-snapshot-controller-0             1/1     Running   0          97m
kube-system         pod/kube-proxy-5nw26                      1/1     Running   0          3h44m
kube-system         pod/kube-proxy-pvtrg                      1/1     Running   0          3h44m
kube-system         pod/kube-proxy-xhx7l                      1/1     Running   0          3h44m

NAMESPACE           NAME                              TYPE           CLUSTER-IP       EXTERNAL-IP                                                                     PORT(S)                      AGE
default             service/kubernetes                ClusterIP      10.100.0.1       <none>                                                                          443/TCP                      3h53m
food-delivery-app   service/mongo                     ClusterIP      None             <none>                                                                          27017/TCP                    69m
food-delivery-app   service/restaurant-service        ClusterIP      10.100.26.93     <none>                                                                          8081/TCP                     14m
food-delivery-app   service/user-service              ClusterIP      10.100.122.160   <none>                                                                          8082/TCP                     14m
kong                service/kong-proxy                LoadBalancer   10.100.120.138   a05712a376e3c45c2be0537c8f28090a-0b68700f6b3ad6df.elb.us-east-1.amazonaws.com   80:32178/TCP,443:32522/TCP   19m
kong                service/kong-validation-webhook   ClusterIP      10.100.4.83      <none>                                                                          443/TCP                      19m
kube-system         service/kube-dns                  ClusterIP      10.100.0.10      <none>                                                                          53/UDP,53/TCP                3h53m

NAMESPACE           NAME                                 READY   UP-TO-DATE   AVAILABLE   AGE
food-delivery-app   deployment.apps/restaurant-service   3/3     3            3           14m
food-delivery-app   deployment.apps/user-service         3/3     3            3           14m
kong                deployment.apps/ingress-kong         1/1     1            1           19m
kube-system         deployment.apps/coredns              2/2     2            2           3h53m
kube-system         deployment.apps/ebs-csi-controller   2/2     2            2           97m

NAMESPACE           NAME                                                   CLASS    HOSTS   ADDRESS                                                                         PORTS   AGE
food-delivery-app   ingress.networking.k8s.io/restaurant-service-ingress   <none>   *       a05712a376e3c45c2be0537c8f28090a-0b68700f6b3ad6df.elb.us-east-1.amazonaws.com   80      14m
food-delivery-app   ingress.networking.k8s.io/user-service-ingress         <none>   *       a05712a376e3c45c2be0537c8f28090a-0b68700f6b3ad6df.elb.us-east-1.amazonaws.com   80      14m

NAMESPACE   NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                                                        STORAGECLASS   REASON   AGE
            persistentvolume/pvc-29b59505-e906-459a-9a38-8c7596566772   1Gi        RWO            Delete           Bound    food-delivery-app/mongodb-persistent-storage-claim-mongo-0   gp2                     68m
            persistentvolume/pvc-486e269d-9f49-4744-b27b-549da3ebe95d   1Gi        RWO            Delete           Bound    food-delivery-app/mongodb-persistent-storage-claim-mongo-2   gp2                     6m50s
            persistentvolume/pvc-a81342a3-affe-4b41-bfc6-230f334af78a   1Gi        RWO            Delete           Bound    food-delivery-app/mongodb-persistent-storage-claim-mongo-1   gp2                     7m13s

NAMESPACE           NAME                                                             STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
food-delivery-app   persistentvolumeclaim/mongodb-persistent-storage-claim-mongo-0   Bound    pvc-29b59505-e906-459a-9a38-8c7596566772   1Gi        RWO            gp2            69m
food-delivery-app   persistentvolumeclaim/mongodb-persistent-storage-claim-mongo-1   Bound    pvc-a81342a3-affe-4b41-bfc6-230f334af78a   1Gi        RWO            gp2            7m19s
food-delivery-app   persistentvolumeclaim/mongodb-persistent-storage-claim-mongo-2   Bound    pvc-486e269d-9f49-4744-b27b-549da3ebe95d   1Gi        RWO            gp2            6m56s
```

# Working Images


## Restaurant-service

![restaurant-service](./images/restaurant-service.png)


## User Service

![user-service](./images/user-service.png)

## Logs

![Logs](./images/kubectl-logs.png)
