/* eslint-disable no-undef */
print(
  "Start #################################################################"
);

db = db.getSiblingDB("restaurantDb");
db.createUser({
  user: "testUser",
  pwd: "simple123",
  roles: [{ role: "readWrite", db: "restaurantDb" }],
});

print("END #################################################################");
